#ifndef SEM_H
#define SEM_H

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include "constante.h"

/* --- Structures --- */

typedef struct{
	/* Id retourner par semget */
	int id;
	/* Clef d'acces au semaphores */
	key_t key;
	/* Nombre de semaphore */
	int nb;
}semaphore;

/* --- Fonctions --- */

/**
 * Creation d'un tableau de semaphores
 * @param semaphore* Le tableau de semaphore
 * @param int La clef
 * @param int Le nombre semaphores
 * @param int* Valeur à initialiser
 * @return boolean Reussite
 */
boolean create_sem(semaphore*, int, int, int*);

/**
 * Récuperation d'un tableau de semaphores
 * @param semaphore* Le tableau de semaphore
 * @param int La clef
 * @return boolean Reussite
 */
boolean get_sem(semaphore*, int);

/**
 * Demande l'acces et decremente le semaphore (Puis je)
 * @param semaphore* Le tableau de semaphore
 * @param int Le numero de la semaphore
 * @return boolean Reussite
 */
boolean P(semaphore*, int);

/**
 * Augmente le compteur d'une semaphore (Vas y)
 * @param semaphore* Le tableau de semaphore
 * @param int Le numero de la semaphore
 * @return boolean Reussite
 */
boolean V(semaphore*, int);

/**
 * Supprime un tableau de semaphores
 * @param semaphore* Le tableau de semaphore
 * @return boolean Reussite
 */
boolean delete_sem(semaphore*);

/**
 * Récupère la valeur d'un semaphore
 * @param semaphore* Le tableau de semaphore
 * @param int Le numero du semaphore dans le tableau
 * @return int La valeur actuel du semaphore
 */
int get_sem_value(semaphore*, int);

#endif
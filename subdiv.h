#ifndef subdiv_h
#define subdiv_h

/* --- Include --- */
#include "constante.h"

/* --- Constante --- */
#define MEMEMPTY 0
#define MEMSET 1

/* --- Structures --- */
typedef struct node node;
struct node{
	int index; //Index dans le tableau mem de subdiv
	int start; //Pos debut (incluse)
	int end; //Pos fin (non incluse)
	int length; //Taille réellement utilisée
	unsigned char type;
	int prev; //Position de noeud d'avant
	int next; //Position du prochain noeud
};

typedef struct{
	node mem[MEM_SIZE / 2];
	boolean use[MEM_SIZE / 2];
}subdiv;

/* --- Fonctions --- */
/**
 * Initialise la structure de gestion
 * @param subdiv* Le gestionnaire de mémoire par subdivision
 */
void ini_subdiv(subdiv*);

/**
 * Ajoute une donnée dans la mémoire
 * @param subdiv* Le gestionnaire de mémoire par subdivision
 * @param char* La mémoire
 * @param char* La clef
 * @param char* La valeur
 * @return Réussite
 */
boolean add_data_subdiv(subdiv*, char*, char*, char*);

/**
 * Ajoute une donnée déjà formatée sour la forme clef=valeur en mémoire
 * @param subdiv* Le gestionnaire de mémoire par subdivision
 * @param char* La mémoire
 * @param char* La donnée
 * @return Réussite
 */
boolean add_fdata_subdiv(subdiv*, char*, char*); //add formated data

/**
 * Récupère une donnée dans la mémoire
 * @param subdiv* Le gestionnaire de mémoire par subdivision
 * @param char* La mémoire
 * @param char* La clef
 * @return Réussite
 */
char* get_data_subdiv(subdiv*, char*, char*);

/**
 * Supprime une donnée de la mémoire
 * @param subdiv* Le gestionnaire de mémoire par subdivision
 * @param char* La mémoire
 * @param char* La clef
 * @return Réussite
 */
boolean remove_data_subdiv(subdiv*, char*, char*);

/**
 * Affiche la répartition de la mémoire
 * @param subdiv* Le gestionnaire de mémoire par subdivision
 */
void show_subdiv(subdiv*);

/**
 * Affiche toutes les données de la mémoire
 * @param subdiv* Le gestionnaire de mémoire par subdivision
 * @param char* La mémoire
 */
void show_data_subdiv(subdiv*, char*);

#endif 

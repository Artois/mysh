/* 
 * File:   execute.c
 * Author: Arthur Brandao
 *
 * Created on 9 novembre 2018
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <wait.h>
#include <string.h>
#include "error.h"
#include "execute.h"

/* --- Extern --- */
extern Error error;
pid_t active = -1;
pid_t last = -1;

/* --- Fonctions publiques --- */
boolean is_executable_file(const char * cmd) {
    int result;
    result = access(cmd, X_OK);
    if(result == ERR){
        return false;
    }
    return true;
}

int exec_shell(char* name, char** argv){
    pid_t pid;  
    int result;
    //Fork pour l'exec
    pid = fork();
    if(pid == ERR){
        addperror("Erreur lors du fork pour la commande execvp");
        return EXIT_FAILURE;
    }
    //Fils
    else if(pid == 0){
        //Reset sortie erreur
        result = redirect_fd(STDERR, error.errfd);
        if(result == ERR){
            adderror("Impossible de redefinir la sortie d'erreur standard");
            exit(EXIT_FAILURE);
        }
        //Execute commande
        execvp(name, argv);
        //Si on arrive ici alors erreur
        char* errmsg = "Erreur dans le fichier execute.c, fonction exec_shell : Impossible d'executer la commande\n";
        if(write(result, errmsg, sizeof(char) * strlen(errmsg)) == ERR){
            addperror("Impossible d'écrire dans le fichier de log");
        }
        fprintf(stderr, "Commande introuvable\n");
        exit(EXIT_FAILURE);
    }
    //Pere
    active = pid;
    wait(&result);
    last = pid;
    active = -1;
    //Retourne retour fils
    if(WIFEXITED(result)){
        return WEXITSTATUS(result);
    }
    return EXIT_FAILURE;
}

int exec_file(char* name, char** argv){
    pid_t pid;  
    int result;
    //Fork pour l'exec
    pid = fork();
    if(pid == ERR){
        addperror("Erreur lors du fork pour la commande execvp");
        return EXIT_FAILURE;
    }
    //Fils
    else if(pid == 0){
        //Reset sortie erreur
        result = redirect_fd(STDERR, error.errfd);
        if(result == ERR){
            adderror("Impossible de redefinir la sortie d'erreur standard");
            exit(EXIT_FAILURE);
        }
        //Execute commande
        execv(name, argv);
        //Si on arrive ici alors erreur
        addperror("Impossible d'executer la commande");
        exit(EXIT_FAILURE);
    }
    //Pere
    active = pid;
    wait(&result);
    last = pid;
    active = -1;
    //Retourne retour fils
    if(WIFEXITED(result)){
        return WEXITSTATUS(result);
    }
    return EXIT_FAILURE;
}
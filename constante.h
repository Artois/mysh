/* 
 * File:   constante.h
 * Author: Arthur Brandao
 *
 * Created on 31 octobre 2018
 */

#ifndef CONSTANTE_H
#define CONSTANTE_H

/* --- General --- */
#define BUFFER_SIZE 512
#define MEM_SIZE 8192
#define SHELL_ERR -1
#define SHELL_FAIL 0
#define SHELL_OK 1

/* --- Fichier --- */
#define STDIN 0
#define STDOUT 1
#define STDERR 2

/* --- Tube --- */
#define TUBE_LECTURE 0
#define TUBE_ECRITURE 1

/* --- Separateur commande --- */
#define SHELL_END 0 //Aucune autre commande après
#define SHELL_NONE 1 //Aucun lien entre les 2 commandes
#define SHELL_IF 2 //La commande suivante s'execute si l'actuel reussis
#define SHELL_ELSE 3 //La commande suivante s'execute si l'actuel échou
#define SHELL_PIPE 4 //Envoi des données de sorties vers la commande suivante

/* --- Redirection (Shell Redirect [Erase]) --- */
#define SHELLR_IN 0 // <
#define SHELLR_OUT 1 // >>
#define SHELLRE_OUT 2 // >
#define SHELLR_ERR 3 // 2>>
#define SHELLRE_ERR 4 // 2>
#define SHELLR_ALL 5 // >>&
#define SHELLRE_ALL 6 // >&

/* --- IPC --- */
#define IPCKEYPATH "/bin/ls"
#define SEMCODE 8426
#define SHMCODEMEM 8520 //Code shm zone des variables
#define SHMCODEGEST 8521 //Code shm du gestionnaire de la zone des variables
#define SHMCODEGLOB 8522 //Code shm des varaibles globales entre tous les processus mysh

/* --- Boolean --- */
#define boolean int
#define true 1
#define false 0

/* --- Null --- */
#define null NULL

#endif /* CONSTANTE_H */


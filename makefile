#
# CONFIGURATION GENERALE
#

EXEC = mysh myls myps
OBJETS = error.o str.o parser.o wildcard.o command.o execute.o sem.o shm.o subdiv.o ipc.o expreg.o variable.o
NOM_PROJET = mini-shell

#
# SUFFIXES
#

.SUFFIXES: .c .o

#
# OBJETS
#

EXEC_O = $(EXEC:=.o)
OBJETS_O = $(OBJETS) $(EXEC_O)

#
# ARGUMENTS ET COMPILATEUR
#

CC = gcc
CCFLAGS_STD = -Wall -O3 -Werror -ansi -pedantic -std=c11
CCFLAGS_DEBUG = -D _DEBUG_
CCFLAGS_VALGRIND = -Wall -O0 -ansi -pedantic -std=c11 -g 
CCFLAGS = $(CCFLAGS_STD)
CCLIBS =

#
# REGLES
#

all: msg $(OBJETS) $(EXEC_O)
	@echo "Creation des executables..."
	@for i in $(EXEC); do \
	$(CC) -o $$i $$i.o $(OBJETS) $(CCLIBS); \
	done
	@echo "Termine."

msg:
	@echo "Creation des objets..."

debug: CCFLAGS = $(CCFLAGS_STD) $(CCFLAGS_DEBUG)
debug: all

valgrind: CCFLAGS = $(CCFLAGS_VALGRIND)
valgrind: all

#
# REGLES PAR DEFAUT
#

.c.o: .h
	@cd $(dir $<) && ${CC} ${CCFLAGS} -c $(notdir $<) -o $(notdir $@)

#
# REGLES GENERALES
#

clean:
	@echo "Suppresion des objets, des fichiers temporaires..."
	@rm -f $(OBJETS) $(EXEC_O)
	@rm -f *~ *#
	@rm -f $(EXEC)
	@rm -f dependances
	@rm -f *.log
	@echo "Termine."

depend:
	@echo "Creation des dependances..."
	@sed -e "/^# DEPENDANCES/,$$ d" makefile > dependances
	@echo "# DEPENDANCES" >> dependances
	@for i in $(OBJETS_O); do \
	$(CC) -MM -MT $$i $(CCFLAGS) `echo $$i | sed "s/\(.*\)\\.o$$/\1.c/"` >> dependances; \
	done
	@cat dependances > makefile
	@rm dependances
	@echo "Termine."
	
rm-log:
	@echo "Suppression log..."
	@rm -f *.log
	@echo "Termine."

#
# CREATION ARCHIVE
#

ARCHIVE_FILES = *

archive: clean
	@echo "Creation de l'archive $(NOM_PROJET)$(shell date '+%y%m%d.tar.gz')..."
	@REP=`basename "$$PWD"`; cd .. && tar zcf $(NOM_PROJET)$(shell date '+%y%m%d.tar.gz') $(addprefix "$$REP"/,$(ARCHIVE_FILES))
	@echo "Termine."

# DEPENDANCES
error.o: error.c str.h error.h
str.o: str.c str.h
parser.o: parser.c error.h str.h wildcard.h constante.h ipc.h sem.h shm.h \
 subdiv.h variable.h parser.h
wildcard.o: wildcard.c error.h wildcard.h constante.h
command.o: command.c command.h constante.h parser.h error.h str.h \
 execute.h ipc.h sem.h shm.h subdiv.h variable.h
execute.o: execute.c error.h execute.h constante.h
sem.o: sem.c error.h sem.h constante.h
shm.o: shm.c error.h shm.h constante.h
subdiv.o: subdiv.c subdiv.h constante.h
ipc.o: ipc.c ipc.h constante.h sem.h shm.h subdiv.h variable.h
expreg.o: expreg.c expreg.h constante.h
variable.o: variable.c str.h expreg.h constante.h variable.h subdiv.h
mysh.o: mysh.c error.h str.h parser.h constante.h mysh.h command.h ipc.h \
 sem.h shm.h subdiv.h execute.h color.h
myls.o: myls.c error.h color.h constante.h
myps.o: myps.c error.h color.h constante.h

# Projet SEC (MySh) Master 1 Artois 2018-2019
### Brandao Arthur (60%) & Maxence Bacquet (40%)



## Utilisation

La compilation du programme s'effectue avec la commande `make` dans le dossier du projet. Il est possible de recréer les dépendances avec la commande `make depend`, et de supprimer les logs d'erreurs avec `make rm-log`. Pour lancer le programme il suffit d'exécuter le fichier ./mysh.

Les commandes externes (myls et myps) sont exécutables sans passer par le programme mysh, mais sont aussi utilisables comme des commandes internes de ce dernier. Il suffit juste de taper le nom de la commande dans un terminal mysh pour l'exécuter.



## Fonctionnalités

Présentes :

- Exécution de commandes
- Séquencement
- Wildcards
- Changement de répertoire
- Sortie du shell (commande exit et Ctrl+C)
- Code de retoure d'un processus (commande status)
- Lister le contenue d'un répertoire (commande myls)
- Etat des processus en cours (commande myps)
- Les tubes
- Les redirections d'entrées et de sorties vers des fichiers
- Premier et arrière plans
- Commande myjobs
- Variables locales et d'environnements

Absentes :

- Commandes myfg, mybg et gestion du Ctrl+Z



## Bugs / Fonctionnalités partiels

Certaines wildcars complexe ne sont pas interprétées (par exemple `[A-Z.]*[^~]`).

Il manque 3 champs dans la commande myps : %CPU, %MEM, TIME. De plus le champ TTY est sous la forme d'un nombre et non d'un texte comme dans la commande `ps -aux`.



## Blagounette

Comment appelle-t-on un combat entre un petit pois et une carotte ?

Un bon duel !


#ifndef STR_H
#define STR_H

#include <string.h>

/**
 * Supprime les espaces avant et après une chaine
 * @param char* La chaine à modifier
 * @return La nouvelle chaine
 */
char* trim(char*);

/**
 * Supprime une suite d'un char avant et après une chaine
 * @param char* La chaine à modifier
 * @param char Le char à supprimer
 * @return La nouvelle chaine
 */
char* mtrim(char*, char);

/**
 * Supprime une suite d'un char avant une chaine
 * @param char* La chaine à modifier
 * @param char Le char à supprimer
 * @return La nouvelle chaine
 */
char* ltrim(char*, char);

/**
 * Supprime une suite d'un char après une chaine
 * @param char* La chaine à modifier
 * @param char Le char à supprimer
 * @return La nouvelle chaine
 */
char* rtrim(char*, char);

#endif /* STR_H */


/* 
 * File:   expreg.h
 * Author: Arthur Brandao
 *
 * Created on 21 décembre 2018
 */

#ifndef EXPREG_H
#define EXPREG_H

/* --- Include --- */
#include <regex.h>
#include "constante.h"

/* --- Structure --- */
typedef struct{
    char* str; //La chaine à vérifier
    int pos; //Position actuel dans la chaine à verifier
    regex_t regex;
    size_t nmatch;
    regmatch_t* pmatch;
}expreg;

/* --- Fonctions --- */
/**
 * Initialise la structure de gestion du regex
 * @param expreg* La structure de l'expression reguliere
 * @param char* La chaine à analyser
 * @param char* Le regex
 * @return Reussite
 */
boolean ini_expreg(expreg*, char*, char*);

/**
 * Recupere la prochaine correspondance dans la chaine
 * @param expreg* La structure de l'expression reguliere
 * @param int* La position de debut de la correspondance
 * @param int* La postion de fin de la correspondance
 * @return La correspondance ou NULL si il n'y en à plus
 */
char* get_match_expreg(expreg*, int*, int*);

/**
 * Vide la structure de l'expression reguliere
 * @param expreg* La structure de l'expression reguliere
 */
void clean_expreg(expreg*);

#endif /* EXPREG_H */


/* 
 * File:   wildcard.h
 * Author: Arthur Brandao
 *
 * Created on 7 novembre 2018
 */

#ifndef WILDCARD_H
#define WILDCARD_H

/* --- Include --- */
#include "constante.h"

/* --- Fonctions publiques ---*/
/**
 * Indique le nombre de fichiers correspond à la sequence dans le dossier de
 * travail courrant
 * @param const char* La sequence
 * @return int Le nombre de fichiers correspondant (0 si aucun ou si le nom du
 * fichier est identique à la sequence)
 */
int wildcard_result(char*);

/**
 * Recupere les fichiers correspondant à une sequence
 * @param const char* La sequence
 * @param int La taille du tableau (parametre suivant)
 * @param char** Tableau qui accueillera le resultat
 * @return int Le nombre d'élement mis dans le tableau (ne depasse jamais la
 * capacité indiqué en paramètre)
 */
int wildcard(char*, int, char**);

/**
 * Insert un tableau à la place d'une valeur d'un autre tableau
 * Tous les tableau passé en pramètre sont vidés et supprimés (ils doivent donc
 * être alloué dynamiquement, de même pour leurs valeurs)
 * @param int La position à remplacer dans le 1er tableau
 * @param char** La tableau à dont un élément est à remplacer
 * @param int La taille du tableau
 * @param char** Le tableau à insérer dans l'autre tableau
 * @param int La taille du tableau à inserer
 * @param int* Variable récupérant la nouvelle taille du tableau (peut etre NULL)
 * @return char** Le nouveau tableau
 */
char** insert_array(int, char**, int, char**, int, int*);

#endif /* WILDCARD_H */


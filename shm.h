/* 
 * File:   shm.h
 * Author: Arthur Brandao
 *
 * Created on 21 décembre 2018
 */

#ifndef SHM_H
#define SHM_H

/* --- Include --- */
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include "constante.h"

/* --- Structure --- */
typedef struct{
    //Id de shmget
    int id;
    //Clef d'accés à la shm
    key_t key;
    //Taille de la zone mémoire
    int size;
    //Pointeur vers la zone mémoire
    void* adr;
}shared_mem;

/* --- Fonctions --- */
/**
 * Création d'un segment de mémoire partagée
 * @param shared_mem* La mémoire partagée
 * @param int La clef
 * @param int La taille de la zone
 * @return boolean Reussite
 */
boolean create_shm(shared_mem*, int, int);

/**
 * Création d'un segment de mémoire partagée
 * @param shared_mem* La mémoire partagée
 * @param int La clef
 * @param int La taille de la zone
 * @return boolean Reussite
 */
boolean get_shm(shared_mem*, int, int);

/**
 * Retire le segment de mémoire partagée
 * @param shared_mem* La mémoire partagée
 * @return boolean Reussite
 */
boolean unset_shm(shared_mem*);

/**
 * Supprime le segment de memoire partagée
 * @param shared_mem* La mémoire partagée
 * @return boolean Reussite
 */
boolean delete_shm(shared_mem*);

#endif /* SHM_H */


/* 
 * File:   ipc.c
 * Author: Arthur Brandao
 *
 * Created on 21 décembre 2018
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "ipc.h"
#include "variable.h"

/* --- Extern --- */
ipc_mysh ipc;
extern int errno;

/* --- Fonctions publiques --- */
boolean setup_ipc(char** envp){
    //boolean setup = true;
    //SHM pour variable environnement
    if(!create_shm(&ipc.memoire, SHMCODEMEM, MEM_SIZE)){
        return false;
    }
    //SHM pour gestionnaire variable env
    if(!create_shm(&ipc.gestionnaire, SHMCODEGEST, sizeof(subdiv))){
        return false;
    }
    //SHM pour variable globale
    if(!create_shm(&ipc.global, SHMCODEGLOB, sizeof(int))){
        return false;
    }
    //Si creation alors on remplie les segments
    if(errno != EEXIST){
        subdiv* sd = (subdiv*) ipc.gestionnaire.adr;
        char* mem = (char*) ipc.memoire.adr;
        int* nb = (int*) ipc.global.adr;
        ini_subdiv(sd);
        int i = 0;
	while(envp[i] != NULL){
            add_fdata_subdiv(sd, mem, envp[i]);
            i++;
	}
        *nb = 0;
    }
    //Semaphore
    int ini[] = {1, 1, 1};
    if(!create_sem(&ipc.sem, SEMCODE, 3, ini)){
        return false;
    }
    return true;
}

boolean end_ipc(){
    //Detache les shm
    if(!unset_shm(&ipc.memoire)){
        return false;
    }
    if(!unset_shm(&ipc.gestionnaire)){
        return false;
    }
    if(!unset_shm(&ipc.global)){
        return false;
    }
    //Tente de supprimer shm
    if(delete_shm(&ipc.global)){
        //Si on est le dernier processus en vie on supprime tous
        if(!delete_shm(&ipc.memoire)){
            return false;
        }
        if(!delete_shm(&ipc.gestionnaire)){
            return false;
        }
        if(!delete_sem(&ipc.sem)){
            return false;
        }
    }
    return true;
}

char* parse_shm_var(char* str){
    //Recup variable globale
    subdiv* sd = (subdiv*) ipc.gestionnaire.adr;
    char* mem = (char*) ipc.memoire.adr;
    int* buf = (int*) ipc.global.adr;
    int nb = *buf;
    char* res;
    //Regarde si l'ecrivain veut ecrire
    P(&ipc.sem, SEMECRIRE);
    V(&ipc.sem, SEMECRIRE);
    //Modification variable globale
    P(&ipc.sem, SEMMUTEX);
    nb++;
    if(nb == 1){
        P(&ipc.sem, SEMWAIT);
    }
    V(&ipc.sem, SEMMUTEX);
    //Lecture
    res = parse_var(str, sd, mem);
    //Modification variable globale
    P(&ipc.sem, SEMMUTEX);
    nb--;
    if(nb == 0){
        V(&ipc.sem, SEMWAIT);
    }
    V(&ipc.sem, SEMMUTEX);
    return res;
}

boolean add_shm_data(char* data){
    //Recup variable globale
    subdiv* sd = (subdiv*) ipc.gestionnaire.adr;
    char* mem = (char*) ipc.memoire.adr;
    boolean res;
    //Indique que l'on veut ecrire
    P(&ipc.sem, SEMECRIRE);
    //Attend que les lecteurs finissent
    P(&ipc.sem, SEMWAIT);
    //Ecriture
    res = add_fdata_subdiv(sd, mem, data);
    //Libere les semaphores
    V(&ipc.sem, SEMWAIT);
    V(&ipc.sem, SEMECRIRE);
    return res;
}

boolean remove_shm_data(char* key){
    //Recup variable globale
    subdiv* sd = (subdiv*) ipc.gestionnaire.adr;
    char* mem = (char*) ipc.memoire.adr;
    boolean res;
    //Indique que l'on veut ecrire
    P(&ipc.sem, SEMECRIRE);
    //Attend que les lecteurs finissent
    P(&ipc.sem, SEMWAIT);
    //Ecriture
    res = remove_data_subdiv(sd, mem, key);
    //Libere les semaphores
    V(&ipc.sem, SEMWAIT);
    V(&ipc.sem, SEMECRIRE);
    return res;
}

void show_shm_data(){
    //Recup variable globale
    subdiv* sd = (subdiv*) ipc.gestionnaire.adr;
    char* mem = (char*) ipc.memoire.adr;
    //Affiche
    show_data_subdiv(sd, mem);
}
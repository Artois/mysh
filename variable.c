/* 
 * File:   variable.c
 * Author: Arthur Brandao
 *
 * Created on 21 décembre 2018
 */

#include <stdio.h>
#include <stdlib.h>
#include "str.h"
#include "expreg.h"
#include "variable.h"

/* --- Extern --- */
subdiv localsd;
char localmem[MEM_SIZE];
boolean ini = false;

/* --- Fonctions publiques --- */
char* parse_var(char* s, subdiv* sd, char* mem) {
    expreg er;
    int length, deb, fin, fin2 = 0, compteur = 0, index = 0;
    boolean first = true;
    char* str, * newstr;
    //Cherche le nombre de clefs dans la chaine
    length = strlen(s);
    ini_expreg(&er, s, "(\\$[[:alnum:]_]+)");
    while ((str = get_match_expreg(&er, &deb, &fin)) != NULL) {
        //1er tour
        if (first) {
            //Si il y a du texte avant 
            if (deb != 0) {
                compteur++;
            }
            compteur++;
            //Garde en memoire la pos de fin
            fin2 = fin;
            first = false;
        }            
        //Autre tours
        else {
            //Si il y a eu du texte entre les deux correspondances
            if (deb != fin2) {
                compteur++;
            }
            compteur++;
            //Garde en memoire la pos de fin
            fin2 = fin;
        }
        free(str);
    }
    //Si on n'est pas à la fin
    if (fin2 != length) {
        compteur++;
    }
    clean_expreg(&er);
    //Création tableau
    char** tab = malloc(sizeof(char*) * compteur);
    //Sécoupe la chaine
    first = true;
    ini_expreg(&er, s, "(\\$[[:alnum:]_]+)");
    while ((str = get_match_expreg(&er, &deb, &fin)) != NULL) {
        //1er tour
        if (first) {
            //Si il y a du texte avant 
            if (deb != 0) {
                tab[index] = malloc(sizeof(char) * (deb + 1));
                memset(tab[index], 0, deb + 1);
                strncpy(tab[index], s, deb);
                index++;
            }
            tab[index] = malloc(sizeof(char) * (fin - deb + 1));
            memset(tab[index], 0, fin - deb + 1);
            strncpy(tab[index], s + deb, fin - deb);
            index++;
            //Garde en memoire la pos de fin
            fin2 = fin;
            first = false;
        }            
        //Autre tours
        else {
            //Si il y a eu du texte entre les deux correspondances
            if (deb != fin2) {
                tab[index] = malloc(sizeof(char) * (deb - fin2 + 1));
                memset(tab[index], 0, deb - fin2 + 1);
                strncpy(tab[index], s + fin2, deb - fin2);
                index++;
            }
            tab[index] = malloc(sizeof(char) * (fin - deb + 1));
            memset(tab[index], 0, fin - deb + 1);
            strncpy(tab[index], s + deb, fin - deb);
            index++;
            //Garde en memoire la pos de fin
            fin2 = fin;
        }
        free(str);
    }
    //Si on n'est pas à la fin
    if (fin2 != length) {
        tab[index] = malloc(sizeof(char) * (length - fin2 + 1));
        memset(tab[index], 0, length - fin2 + 1);
        strncpy(tab[index], s + fin2, length - fin2);
        index++;
    }
    clean_expreg(&er);
    //Remplace les clef par leurs valeurs
    for(int i = 0; i < compteur; i++){
        if(tab[i][0] == '$'){
            char* val = get_data_subdiv(sd, mem, tab[i] + 1);
            if(val != NULL){
                free(tab[i]);
                tab[i] = val;
            }
        }
    }
    //Reconstruit la chaine
    length = 0;
    for(int i = 0; i < compteur; i++){
        length += strlen(tab[i]);
    }
    newstr = malloc(sizeof(char) * (length + 1));
    memset(newstr, 0, length + 1);
    index = 0;
    for(int i = 0; i < compteur; i++){
        length = strlen(tab[i]);
        for(int j = 0; j < length; j++){
            newstr[index] = tab[i][j];
            index++;
        }
        free(tab[i]);
    }
    free(tab);
    return newstr;
}

char* parse_local_var(char* str){
    if(!ini){
        int length = strlen(str) + 1;
        char* newstr = malloc(sizeof(char) * length);
        memset(newstr, 0, length);
        strncpy(newstr, str, length - 1);
        return newstr;
    }
    return parse_var(str, &localsd, localmem);
}

boolean add_local_data(char* data){
    if(!ini){
        ini_subdiv(&localsd);
        ini = true;
    }
    return add_fdata_subdiv(&localsd, localmem, data);
}

boolean remove_local_data(char* key){
    if(!ini){
        return false;
    }
    return remove_data_subdiv(&localsd, localmem, key);
}

void show_local_data(){
    show_data_subdiv(&localsd, localmem);
}
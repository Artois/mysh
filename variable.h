/* 
 * File:   variable.h
 * Author: Arthur Brandao
 *
 * Gestion des variables locales
 * 
 * Created on 21 décembre 2018
 */

#ifndef VARIABLE_H
#define VARIABLE_H

/* --- Include --- */
#include "constante.h"
#include "subdiv.h"

/* --- Fonctions --- */
/**
 * Remplace les variables par leur valeur dans une chaine
 * @param char* La chaine à traiter
 * @param subdiv* Le gestionnaire de memoir epar subdivision ou sont stocker les valeur
 * @param char* La mémoire ou sont les valeurs
 * @return La chaine avec les valeurs replacées
 */
char* parse_var(char*, subdiv*, char*);

/**
 * Remplace les variables d'une chaine par leurs valeurs
 * @param char* La chaine à analyser
 * @return La chaine avec les variables remplacées par leur valeurs
 */
char* parse_local_var(char*);

/**
 * Ajoute une données dans la mémoire locale
 * @param char* Les données à ajouter
 * @return Reussite
 */
boolean add_local_data(char*);

/**
 * Supprime une donnée dans la mémoire locale
 * @param char* La clef
 * @return Reussite
 */
boolean remove_local_data(char*);

/**
 * Affiche les variables dans la mémoire
 */
void show_local_data();

#endif /* VARIABLE_H */


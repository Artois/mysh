/* 
 * File:   expreg.c
 * Author: Arthur Brandao
 *
 * Created on 21 décembre 2018
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "expreg.h"

/* --- Fonctions publiques --- */
boolean ini_expreg(expreg* er, char* str, char* regex) {
    //Setup regex
    if (regcomp(&er->regex, regex, REG_EXTENDED) != 0) {
        return false;
    }
    //Copie la chaine
    int length = strlen(str);
    er->str = malloc(sizeof (char) * (length + 1));
    memset(er->str, 0, length + 1);
    strncpy(er->str, str, length);
    //Setup structure
    er->pos = 0;
    er->nmatch = er->regex.re_nsub;
    er->pmatch = malloc(sizeof (regmatch_t) * er->nmatch);
    return true;
}

char* get_match_expreg(expreg* er, int* deb, int* fin) {
    if (regexec(&er->regex, er->str + er->pos, er->nmatch, er->pmatch, 0) != 0) {
        if(deb != NULL){
            *deb = -1;
        }
        if(fin != NULL){
            *fin = -1;
        }
        return NULL;
    }
    //Recup info regex
    int start = er->pmatch[0].rm_so;
    int end = er->pmatch[0].rm_eo;
    int length = end - start;
    //Indique les positions
    if(deb != NULL){
        *deb = er->pos + start;
    }
    if(fin != NULL){
        *fin = er->pos + end;
    }
    //Recup la chaine
    char* str;
    str = malloc(sizeof(char) * (length + 1));
    memset(str, 0, length + 1);
    strncpy(str, &er->str[er->pos + start], length);
    //On avance dans la chaine
    er->pos += end;
    return str;
}

void clean_expreg(expreg* er) {
    regfree(&er->regex);
    free(er->str);
    free(er->pmatch);
}
/* 
 * File:   mysh.h
 * Author: Arthur Brandao
 *
 * Created on 6 novembre 2018
 */

#ifndef MYSH_H
#define MYSH_H

/* --- Include --- */
#include "command.h"

/* --- Fonctions --- */
/**
 * Affiche le dossier de travail actuel
 * @param char* Chaine à afficher avant le dossier
 * @param char* Chaine à afficher après le dossier
 */
void show_current_dir(const char*, const char*);

/**
 * Execute l'ensemble des commandes d'une ligne
 * @param CommandTab La ligne de commande parser
 * @return Result
 */
int run(CommandTab, int*);

/**
 * Gestionnaire de signaux
 * @param int Le type de singal reçu
 */
void handler(int);

#endif /* MYSH_H */


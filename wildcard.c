/* 
 * File:   wildcard.c
 * Author: Arthur Brandao
 *
 * Created on 7 novembre 2018
 */

#define _DEFAULT_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fnmatch.h>
#include <string.h>
#include "error.h"
#include "wildcard.h"

/* --- Fonctions privées --- */

/**
 * Determine la position de separation entre le chemin et la wildcard
 * @param char* La chaine
 * @return int Position dans la chaine
 */
int wilcard_begin(const char* seq) {
    int pos = 0, compteur = 1;
    while (*seq) {
        if (*seq == '/') {
            pos = compteur;
        }
        seq++;
        compteur++;
    }
    return pos;
}

/* --- Fonctions publiques --- */
int wildcard_result(char* seq) {
    //Declaration variable
    struct dirent **namelist;
    int nbFile, nbRes = 0, wcpos;
    char* wc, *path;
    //Trouve la wildcard
    wcpos = wilcard_begin(seq);
    if (!wcpos) {
        path = malloc(sizeof (char) * 2);
        strcpy(path, ".");
        wc = seq;
    } else {
        path = malloc(sizeof (char) * wcpos);
        memset(path, 0, wcpos);
        strncpy(path, seq, wcpos - 1); //Pour ne pas prendre le / final
        wc = seq + wcpos;
    }
    //Recup la liste des fichiers dans le dossier courant
    nbFile = scandir(path, &namelist, 0, alphasort);
    if (nbFile < 0) {
        addperror("Erreur scandir()");
        return ERR;
    }
    //Parcours chaque fichier pour compter le nombre de resultat
    while (nbFile--) {
        //Si c'est bien un fichier (et non un dossier)
        if (namelist[nbFile]->d_type == DT_REG) {
            //Test par rapport au wildcard
            if (fnmatch(wc, namelist[nbFile]->d_name, 0) == 0) {
                //Regarde si le resultat est bien different de la wildcard
                if (strcmp(wc, namelist[nbFile]->d_name) != 0) {
                    nbRes++;
                }
            }
        }
        free(namelist[nbFile]);
    }
    free(namelist);
    //Retour
    return nbRes;
}

int wildcard(char* seq, int size, char** result) {
    //Declaration variable
    struct dirent **namelist;
    int nbFile, nbRes = 0, wcpos;
    char* wc, *path;
    //Verification parametre
    if (size < 1) {
        return -1;
    }
    //Trouve la wildcard
    wcpos = wilcard_begin(seq);
    if (!wcpos) {
        path = malloc(sizeof (char) * 2);
        strcpy(path, ".");
        wc = seq;
    } else {
        path = malloc(sizeof (char) * wcpos);
        memset(path, 0, wcpos);
        strncpy(path, seq, wcpos - 1);
        wc = seq + wcpos;
    }
    //Recup la liste des fichiers dans le dossier courant
    nbFile = scandir(path, &namelist, 0, alphasort);
    if (nbFile < 0) {
        addperror("Erreur scandir()");
        return ERR;
    }
    //Parcours chaque fichier pour ajouter les resultats à result
    int i = 0;
    while (nbFile--) {
        //Si c'est bien un fichier (et non un dossier)
        if (namelist[nbFile]->d_type == DT_REG) {
            //Test par rapport au wildcard
            if (fnmatch(wc, namelist[nbFile]->d_name, 0) == 0) {
                //Si il y a un chemin
                if (wcpos) {
                    result[i] = malloc(sizeof (char) * (strlen(path) + strlen(namelist[nbFile]->d_name) + 2));
                    memset(result[i], 0, strlen(path) + strlen(namelist[nbFile]->d_name) + 2);
                    sprintf(result[i++], "%s/%s", path, namelist[nbFile]->d_name);
                } else {
                    result[i] = malloc(sizeof (char) * (strlen(namelist[nbFile]->d_name) + 1));
                    memset(result[i], 0, strlen(namelist[nbFile]->d_name) + 1);
                    sprintf(result[i++], "%s", namelist[nbFile]->d_name);
                }
                nbRes++;
            }
        }
        //Nettoyage
        free(namelist[nbFile]);
        //Si il n'y a plus de place dans le tableau on s'arrete
        if (i == size) {
            break;
        }
    }
    free(namelist);
    //Retourne
    return nbRes;
}

char** insert_array(int pos, char** array, int arraysize, char** insert, int insertsize, int* newsize) {
    //Erreur parametre
    if (pos < 0 || arraysize < 1 || insertsize < 1 || pos >= arraysize) {
        if (newsize != NULL) {
            *newsize = ERR;
        }
        return NULL;
    }
    //Si une seul valeur à insérer
    if (insertsize == 1) {
        //Vide la chaine precedente
        free(array[pos]);
        //Remplace
        array[pos] = malloc(strlen(insert[0]) * sizeof (char));
        strcpy(array[pos], insert[0]);
        //Nettoyage
        free(insert[0]);
        free(insert);
        //Retourne le tableau
        if (newsize != NULL) {
            *newsize = arraysize;
        }
        return array;
    }
    //Sinon generation d'un nouveau tableau
    char ** newarray;
    int size = arraysize + insertsize - 1, i;
    newarray = malloc(sizeof (char*) * size);
    //Ajout des elements avant la postion
    for (i = 0; i < pos; i++) {
        //Si l'element est null
        if (array[i] == NULL) {
            newarray[i] = NULL;
        }            //Sinon on le copie
        else {
            newarray[i] = malloc(strlen(array[i]) * sizeof (char));
            strcpy(newarray[i], array[i]);
            free(array[i]);
        }
    }
    //Ajout des nouveaux elements
    for (int j = 0; j < insertsize; j++, i++) {
        //Si l'element est null
        if (insert[j] == NULL) {
            newarray[i] = NULL;
        }            //Sinon on le copie
        else {
            newarray[i] = malloc(strlen(insert[j]) * sizeof (char));
            strcpy(newarray[i], insert[j]);
            free(insert[j]);
        }
    }
    //Ajout fin
    for (int j = pos + 1; j < arraysize; j++, i++) {
        //Si l'element est null
        if (array[j] == NULL) {
            newarray[i] = NULL;
        }            //Sinon on le copie
        else {
            newarray[i] = malloc(strlen(array[j]) * sizeof (char));
            strcpy(newarray[i], array[j]);
            free(array[j]);
        }
    }
    //Nettoyage et changement tableau
    free(array[pos]);
    free(array);
    free(insert);
    //Indique la nouvelle taille
    if (newsize != NULL) {
        *newsize = size;
    }
    //Retourne le nombre d'element
    return newarray;
}
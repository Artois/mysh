/* 
 * File:   parser.h
 * Author: Arthur Brandao
 *
 * Created on 31 octobre 2018
 */

#ifndef PARSER_H
#define PARSER_H

/* --- Include --- */
#include "constante.h"

/* --- Structure --- */
typedef struct {
    char* cmd; //La commande en string
    char* name; //Le nom de la commande
    int argc; //Le nombre d'argument
    char** argv; //Les arguments
    int input; //Descripteur de fichier d'entré
    int output; //Descripteur de fichier de sortie
    int error; //Descripteur de fihier d'erreur
    int next; //Lien avec la prochaine commande
} Command;

typedef struct {
    char* line; //La ligne de commande
    Command** cmd; //Tableau avec toutes les commandes
    int length; //Taille du tableau
    boolean bck; //Si l'ensemble de commande se fait en fond
} CommandTab;

/* --- Fonctions --- */

/**
 * Parse une ligne de commande (separe les commandes et repere le lien entre elle)
 * @param CommandTab* La structure contenant le resultat
 * @param char* La ligne de commande à parser
 * @return int SHELL_OK si réussite, SHELL_ERR sinon
 */
int parse_line(CommandTab*, char*);

/**
 * Parse une commande (repere changement entrée sortie, wildcard, découpe les
 * arguments)
 * @param Command* La structure commande à parser (initialisée dans une 
 * CommandTab par la fonction parse_line)
 * @return int SHELL_OK si réussite, SHELL_ERR sinon
 */
int parse_command(Command*);

/**
 * Parse toutes les commandes d'un CommandTab
 * @param CommandTab* La structure contenant les commandes initialisée dans
 * la fonctions parse_line)
 * @return int SHELL_OK si réussite, SHELL_FAIL si le parse d'une des commandes
 * échoue
 */
int parse_all_command(CommandTab*);

/**
 * Vide un structure CommandTab
 * @param CommandTab* La structure à nettoyer
 */
void clean_command(CommandTab*);

#endif /* PARSER_H */


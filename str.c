#include <stdio.h>
#include <stdlib.h>
#include "str.h"

char* trim(char* str){
    return ltrim(rtrim(str, ' '), ' ');
}

char* mtrim(char* str, char mask){
    return ltrim(rtrim(str, mask), mask);
}

char* ltrim(char* str, char mask){
    //Variable
    int cmpt = 0;
    char* res;
    //Compte le nombre d'espace
    while(str[cmpt] == mask){
        cmpt++;
    }
    //Si aucun espace au debut
    if(cmpt == 0){
        res = malloc(sizeof(char) * (strlen(str) + 1));
        memset(res, 0, strlen(str) + 1);
        strcpy(res, str);
        return res;
    }
    //Sinon creation nouvelle chaine
    res = malloc(sizeof(char) * (strlen(str) - cmpt + 1));
    memset(res, 0, strlen(str) - cmpt + 1);
    for(int i = 0, j = cmpt; j < (strlen(str)); i++, j++){
        res[i] = str[j];
    }
    //Retour nouvelle chaine
    return res;
}

char* rtrim(char* str, char mask){
    //Variable
    int cmpt = strlen(str) - 1;
    char* res;
    //Compte le nombre d'espace
    while(str[cmpt] == mask){
        cmpt--;
    }
    //Si aucun espace au debut
    if(cmpt == strlen(str) - 1){
        res = malloc(sizeof(char) * (strlen(str) + 1));
        memset(res, 0, strlen(str) + 1);
        strcpy(res, str);
        return res;
    }
    cmpt++;
    //Sinon creation nouvelle chaine
    res = malloc(sizeof(char) * (cmpt + 2));
    memset(res, 0, cmpt + 2);
    for(int i = 0; i < cmpt; i++){
        res[i] = str[i];
    }
    //Retour nouvelle chaine
    return res;
}
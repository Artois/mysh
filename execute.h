/* 
 * File:   execute.h
 * Author: Arthur Brandao
 *
 * Created on 9 novembre 2018
 */

#ifndef EXECUTE_H
#define EXECUTE_H

/* --- Include --- */
#include <sys/types.h>
#include "constante.h"

/* --- Extern --- */
extern pid_t active;
extern pid_t last;

/* --- Fonctions publiques --- */
/**
 * Indique si un fichier est executable
 * @param char* Chemin vers le fichier
 * @return Vrai/Faux
 */
boolean is_executable_file(const char *);

/**
 * Execute une commande shell
 * @param char* Nom de la commande
 * @param char** Arguments (argv)
 * @return Status de la commande
 */
int exec_shell(char*, char**);

/**
 * Execute un programme
 * @param char* Nom du fichier executable
 * @param char** Arguments (argv)
 * @return Status du programme
 */
int exec_file(char*, char**);

#endif /* EXECUTE_H */


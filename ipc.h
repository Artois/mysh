/* 
 * File:   ipc.h
 * Author: Arthur Brandao
 *
 * Created on 21 décembre 2018
 */

#ifndef IPC_H
#define IPC_H

/* --- Include --- */
#include "constante.h"
#include "sem.h"
#include "shm.h"
#include "subdiv.h"

/* --- Constantes --- */
#define SEMMUTEX 0 //Mutex d'accès aux variables globales
#define SEMECRIRE 1 //Sem pour indiquer qu'un processus veut ecrire
#define SEMWAIT 2 //Sem pour faire attendre l'ecrivain

/* --- Structure --- */
typedef struct{
    shared_mem memoire;
    shared_mem gestionnaire;
    shared_mem global;
    semaphore sem;
}ipc_mysh;

/* --- Extern --- */
extern ipc_mysh ipc;

/* --- Fonctions --- */
/**
 * Création/Récupération SHM et SEM
 * @param char** Envp pour la création
 * @return Réussite
 */
boolean setup_ipc(char**);

/**
 * Destruction des SHM et SEM
 * @return Reussite
 */
boolean end_ipc();

/**
 * Remplace les variables d'une chaine par leurs valeurs
 * @param char* La chaine à analyser
 * @return La chaine avec les variables remplacées par leur valeurs
 */
char* parse_shm_var(char*);

/**
 * Ajoute une données dans le segment de mémoire
 * @param char* Les données à ajouter
 * @return Reussite
 */
boolean add_shm_data(char*);

/**
 * Supprime une donnée dans le segment de mémoire
 * @param char* La clef
 * @return Reussite
 */
boolean remove_shm_data(char*);

/**
 * Affiche les variables dans la mémoire
 */
void show_shm_data();

#endif /* IPC_H */


/* 
 * File:   command.h
 * Author: Arthur Brandao
 *
 * Created on 9 novembre 2018
 */

#ifndef COMMAND_H
#define COMMAND_H

/* --- Include --- */
#include <sys/types.h>
#include "constante.h"
#include "parser.h"

/* --- Structure --- */
typedef struct pid_node pid_node;
struct pid_node{
    pid_t pid;
    int job;
    char* cmd;
    pid_node* prev;
    pid_node* next;
};
typedef struct{
    pid_node* first;
    pid_node* last;
}pid_list;

/* --- Extern --- */
extern char* cmdlist[];
extern boolean exitsh;
extern int status_cmd;
extern int result_cmd;
extern char base_path[];
extern pid_list pidlist;

/* --- Fonctions --- */
/**
 * Initialise la liste de PID
 * @param pid_list* La liste de PID
 */
void ini_pid_list(pid_list*);

/**
 * Ajoute un PID dans la liste
 * @param pid_list* La liste de PID
 * @param pid_t Le pid
 * @param int Le numero de job
 * @param char* Le nom de la commande
 * @return Le noeud crée
 */
pid_node* add_pid(pid_list*, pid_t, int, char*);

/**
 * Cherche un PID dans la liste
 * @param pid_list* La liste de PID
 * @param pid_t le PID
 * @return Le noeud du pid
 */
pid_node* search_pid(pid_list*, pid_t);

/**
 * Supprime un PID de la liste
 * @param pid_list* La liste de PID
 * @param pid_node* Le noeud à supprimer
 */
void remove_pid(pid_list*, pid_node*);

/**
 * Vide la liste de PID
 * @param pid_list* La liste de PID
 */
void clean_pid(pid_list*);

/**
 * Indique si un commande est une commande interne
 * @param char* La commande
 * @return Truu : oui / False : non
 */
boolean is_internal_cmd(const char*);

/**
 * Lance une commande intene
 * @param Commande* La structure de la commande
 * @return Le statut
 */
int launch_internal_command(Command*);

/* --- Commandes --- */
/**
 * Change le dossier de travail actuel
 * @param int argc
 * @param char** argv
 * @return Statut
 */
int cd(int, char**);

/**
 * Quitte le shell
 * @param int argc
 * @param char** argv
 * @return Statut
 */
int exit_sh(int, char**);

/**
 * Statut de la dernière éxecution
 * @param int argc
 * @param char** argv
 * @return Statut
 */
int status(int, char**);

/**
 * AJoute une variable d'environnement
 * @param int argc
 * @param char** argv
 * @return Statut
 */
int set_env(int, char**);

/**
 * Supprime une variable d'environnement
 * @param int argc
 * @param char** argv
 * @return Statut
 */
int unset_env(int, char**);

/**
 * Ajoute une variable locale
 * @param int argc
 * @param char** argv
 * @return Statut
 */
int set_local(int, char**);

/**
 * Supprime une variable locale
 * @param int argc
 * @param char** argv
 * @return Statut
 */
int unset_local(int, char**);

/**
 * Execute le programme myls
 * @param int argc
 * @param char** argv
 * @return Statut
 */
int myls(int, char**);

/**
 * Execute le programma myps
 * @param int argc
 * @param char** argv
 * @return Statut
 */
int myps(int, char**);

/**
 * Affiche tous les jobs
 * @param int argc
 * @param char** argv
 * @return Statut
 */
int myjobs(int, char**);

#endif /* COMMAND_H */

